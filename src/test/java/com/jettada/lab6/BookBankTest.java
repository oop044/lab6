package com.jettada.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess() {
        BookBank book = new BookBank("jettada", 100);
        book.withdraw(50);
        assertEquals(50, book.getBalance(),0.0001);
    }

    @Test
    public void shouldWithOverBarlance() {
        BookBank book = new BookBank("jettada", 100);
        book.withdraw(150);
        assertEquals(100, book.getBalance(),0.0001);
    }
    @Test
    public void shouldWithNegativeNumber() {
        BookBank book = new BookBank("jettada", 100);
        book.withdraw(-100);
        assertEquals(100, book.getBalance(),0.0001);
    }
    @Test
    public void shouldDispositSuccess(){
        BookBank book = new BookBank("jettada", 100);
        book.deposit(1000);
        assertEquals(1100, book.getBalance(),0.0001);
    }
    @Test 
    public void shouldDispositNegativeNumber() {
        BookBank book = new BookBank("jettada", 100);
        book.deposit(-100);
        assertEquals(100, book.getBalance(),0.0001);
    }
}
