package com.jettada.lab6;

public class RobotApp2 {
    public static void main(String[] args) {
        Robot body = new Robot("Body", 'B', 1, 0);
        Robot peter = new Robot("Petter", 'P', 10, 10);
        body.print();
        body.right();
        body.print();
        peter.print();

        for (int y = Robot.MIN_Y; y <= Robot.MAX_Y; y++) {
            for (int x = Robot.MIN_X; x <= Robot.MAX_X; x++) {
                if (body.getX() == x && body.getY() == y) {
                    System.out.print(body.getSymbol());
                } else if (peter.getX() == x && peter.getY() == y) {
                    System.out.print(peter.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
}