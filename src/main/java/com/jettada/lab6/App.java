package com.jettada.lab6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank jettada = new BookBank("Jettada",50);
        jettada.print();
        BookBank prayut = new BookBank("prayut",100000.0);
        prayut.print();
        prayut.withdraw(40000.0) ;
        prayut.print();
        
        jettada.deposit(40000.0);
        jettada.print();

        BookBank prawit = new BookBank("prawit",1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();
    }
}
